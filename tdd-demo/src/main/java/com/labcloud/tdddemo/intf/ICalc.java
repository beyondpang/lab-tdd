package com.labcloud.tdddemo.intf;

/**
 * @Author 苏建锋
 * @create 2019-01-09 23:38
 */
public interface ICalc {
    public int add(int a, int b);
}
