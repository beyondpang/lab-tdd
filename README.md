# tdd(测试驱动开发）演示案例

#### 介绍
通过一个小型的电商系统，来演示TDD开发过程

#### TDD开发流程
![软件架构说明](https://images.gitee.com/uploads/images/2019/0101/002646_a5e6311b_2301840.png "屏幕截图.png")

#### 使用技术框架
IDEA<br>
SpringBoot 2.1.1.RELEASE<br>
Junit4.12<br>
myBatis<br>

#### 安装教程

克隆或fork代码： https://gitee.com/sujianfeng/lab-tdd

#### 使用说明

查阅根目录下的《演示说明书.docx》

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)